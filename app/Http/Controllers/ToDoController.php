<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ToDo;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\ToDoRequest;

class ToDoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $todo = ToDo::where('name', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $todo = ToDo::paginate($perPage);
        }

        return view('to-do.index', compact('todo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('to-do.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ToDoRequest $request)
    {

        $todo = new ToDo;
        $todo->name = $request->name;
        $todo->$date = $request->$date;
        $todo->$description = $request->$description;
        $todo->user_id = Auth::user()->id;
        $todo->save();

        return redirect('to-do')->with('flash_message', 'ToDo added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $todo = ToDo::findOrFail($id);

        return view('to-do.show', compact('todo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $todo = ToDo::findOrFail($id);

        return view('to-do.edit', compact('todo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ToDoRequest $request, $id)
    {

        $requestData = $request->all();

        $todo = ToDo::findOrFail($id);
        $todo->name = $request->name;
        $todo->state = $request->state;
        $todo->$date = $request->$date;
        $todo->$description = $request->$description;
        $todo->user_id = Auth::user()->id;
        $todo->save();

        return redirect('to-do')->with('flash_message', 'ToDo updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ToDo::destroy($id);

        return redirect('to-do')->with('flash_message', 'ToDo deleted!');
    }
}
